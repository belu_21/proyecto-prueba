DROP TABLE IF EXISTS factura;

CREATE TABLE factura (
    id   BIGINT NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    nro_cuenta VARCHAR2(12),
    fecha_emision DATE,
    contrato  NUMERIC(12,0),
    estado   CHAR(2),
    nombre  VARCHAR2(100),
    ci   VARCHAR2(12),
    nro_factura VARCHAR2(20),
    monto  NUMERIC(12,2)
);
