package example.micronaut;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Introspected 
public class FacturaUpdateCommand {
    @NotNull
    private final Long id;

    @NotBlank
    private final String nro_cuenta;

    public FacturaUpdateCommand(Long id, String nro_cuenta) {
        this.id = id;
        this.nro_cuenta = nro_cuenta;
    }

    public Long getId() {
        return id;
    }

    public String getNro_cuenta() {
        return nro_cuenta;
    }

}