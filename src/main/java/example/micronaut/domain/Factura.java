package example.micronaut.domain;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import java.util.Date;

import javax.validation.constraints.NotNull;

@MappedEntity
public class Factura{
    @Id
    @GeneratedValue(GeneratedValue.Type.AUTO)
    private Long id;

    private String nro_cuenta;
    private Date fecha_emision;
    private Long contrato;
    private char estado;
    private String nombre;
    private String ci;
    private String nro_factura;
    private Long monto;


public Long getId() {
			return this.id;
		}
public void setId(Long input) {
			this.id = input;
			}

public String getNro_cuenta() {
			return this.nro_cuenta;
		}
public void setNro_cuenta(String input) {
			this.nro_cuenta = input;
			}


public Date getFecha_emision() {
			return this.fecha_emision;
		}
public void setFecha_emision(Date input) {
			this.fecha_emision = input;
			}
public Long getContrato() {
			return this.contrato;
		}
public void setContrato(Long input) {
			this.contrato = input;
			}
public char getEstado() {
			return this.estado;
		}
public void setEstado(char input) {
			this.estado = input;
			}
 
public String getNombre() {
			return this.nombre;
		}
public void setNombre(String input) {
			this.nombre = input;
			}
public String getCi() {
			return this.ci;
		}
public void setCi(String input) {
			this.ci = input;
			}
public String getNro_factura() {
			return this.nro_factura;
		}
public void setNro_factura(String input) {
			this.nro_factura = input;
			}

public Long getMonto() {
			return this.id;
		}
public void setMonto(Long input) {
			this.monto = input;
			}
@Override
    public String toString() {
        return "Factura{" +
                "id=" + id +
                ", nro_cuenta='" + nro_cuenta + '\'' +
                ", nombre='" + nombre + '\'' +
                ", ci='" + ci + '\'' +
                ", nro_factura='" + nro_factura + '\'' +
                ", fecha de emision='" + fecha_emision + '\'' +
                '}';
    }


}