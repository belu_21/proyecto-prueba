package example.micronaut;

import example.micronaut.domain.Factura;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@MicronautTest 
public class FacturaControllerTest {

    @Inject
    @Client("/")
    HttpClient client; 

    @Test
    public void testFindNonExistingFacturaReturns404() {
        HttpClientResponseException thrown = assertThrows(HttpClientResponseException.class, () -> {
            client.toBlocking().exchange(HttpRequest.GET("/facturas/99"));
        });

        assertNotNull(thrown.getResponse());
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @Test
    public void testFacturaCrudOperations() {

        List<Long> facturaIds = new ArrayList<>();

        HttpRequest<?> request = HttpRequest.POST("/facturas", Collections.singletonMap("name", "DevOps")); 
        HttpResponse<?> response = client.toBlocking().exchange(request);
        facturaIds.add(entityId(response));

        assertEquals(HttpStatus.CREATED, response.getStatus());

        request = HttpRequest.POST("/facturas", Collections.singletonMap("name", "Microservices")); 
        response = client.toBlocking().exchange(request);

        assertEquals(HttpStatus.CREATED, response.getStatus());

        Long id = entityId(response);
        facturaIds.add(id);
        request = HttpRequest.GET("/facturas/" + id);

        Factura factura = client.toBlocking().retrieve(request, Factura.class); 

        assertEquals("Microservices", factura.getName());

        request = HttpRequest.PUT("/facturas", new FacturaUpdateCommand(id, "Micro-services"));
        response = client.toBlocking().exchange(request);  

        assertEquals(HttpStatus.NO_CONTENT, response.getStatus());

        request = HttpRequest.GET("/facturas/" + id);
        factura = client.toBlocking().retrieve(request, Factura.class);
        assertEquals("Micro-services", factura.getName());

        request = HttpRequest.GET("/facturas/list");
        List<Factura> facturas = client.toBlocking().retrieve(request, Argument.of(List.class, Factura.class));

        assertEquals(2, facturas.size());

        request = HttpRequest.POST("/facturas/ex", Collections.singletonMap("name", "Microservices")); 
        response = client.toBlocking().exchange(request);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatus());

        request = HttpRequest.GET("/facturas/list");
        facturas = client.toBlocking().retrieve(request, Argument.of(List.class, Factura.class));

        assertEquals(2, facturas.size());

        request = HttpRequest.GET("/facturas/list?size=1");
        facturas = client.toBlocking().retrieve(request, Argument.of(List.class, Factura.class));

        assertEquals(1, facturas.size());
        assertEquals("DevOps", facturas.get(0).getName());

        request = HttpRequest.GET("/facturas/list?size=1&sort=name,desc");
        facturas = client.toBlocking().retrieve(request, Argument.of(List.class, Factura.class));

        assertEquals(1, facturas.size());
        assertEquals("Micro-services", facturas.get(0).getName());

        request = HttpRequest.GET("/facturas/list?size=1&page=2");
        facturas = client.toBlocking().retrieve(request, Argument.of(List.class, Factura.class));

        assertEquals(0, facturas.size());

        // cleanup:
        for (Long facturaId : facturaIds) {
            request = HttpRequest.DELETE("/facturas/" + facturaId);
            response = client.toBlocking().exchange(request);
            assertEquals(HttpStatus.NO_CONTENT, response.getStatus());
        }
    }

    protected Long entityId(HttpResponse<?> response) {
        String path = "/facturas/";
        String value = response.header(HttpHeaders.LOCATION);
        if (value == null) {
            return null;
        }
        int index = value.indexOf(path);
        if (index != -1) {
            return Long.valueOf(value.substring(index + path.length()));
        }
        return null;
    }
}