package example.micronaut;

import example.micronaut.domain.Factura;
import io.micronaut.data.exceptions.DataAccessException;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.Status;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)  
@Controller("/facturas")  
public class FacturaController {

    protected final FacturaRepository FacturaRepository;

    public FacturaController(FacturaRepository facturaRepository) { 
        this.facturaRepository = facturaRepository;
    }

    @Get("/{id}") 
    public Optional<Factura> show(Long id) {
        return facturaRepository
                .findById(id); 
    }

    @Put 
    public HttpResponse update(@Body @Valid FacturaUpdateCommand command) { 
        facturaRepository.update(command.getId(), command.getName());
        return HttpResponse
                .noContent()
                .header(HttpHeaders.LOCATION, location(command.getId()).getPath()); 
    }

    @Get("/list") 
    public List<Factura> list(@Valid Pageable pageable) { 
        return facturaRepository.findAll(pageable).getContent();
    }

    @Post 
    public HttpResponse<Factura> save(@Body("nro_factura") @NotBlank String nro_factura) {
        Factura factura = facturaRepository.save(nro_factura);

        return HttpResponse
                .created(factura)
                .headers(headers -> headers.location(location(factura.getId())));
    }

    @Post("/ex") 
    public HttpResponse<Factura> saveExceptions(@Body @NotBlank String nro_factura) {
        try {
            Factura factura = facturaRepository.saveWithException(nro_factura);
            return HttpResponse
                    .created(factura)
                    .headers(headers -> headers.location(location(factura.getId())));
        } catch(DataAccessException e) {
            return HttpResponse.noContent();
        }
    }

    @Delete("/{id}") 
    @Status(HttpStatus.NO_CONTENT)
    public void delete(Long id) {
        facturaRepository.deleteById(id);
    }

    protected URI location(Long id) {
        return URI.create("/facturas/" + id);
    }

    protected URI location(Factura factura) {
        return location(factura.getId());
    }
}