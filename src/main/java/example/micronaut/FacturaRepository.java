package example.micronaut;

import example.micronaut.domain.Factura;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.exceptions.DataAccessException;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.PageableRepository;

import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JdbcRepository(dialect = Dialect.ORACLE) 
public interface FacturaRepository extends PageableRepository<Factura, Long> { 

    Factura save(@NonNull @NotBlank String nro_cuenta);

    @Transactional
    default Factura saveWithException(@NonNull @NotBlank String nro_cuenta) {
        save(nro_cuenta);
        throw new DataAccessException("test exception");
    }

    long update(@NonNull @NotNull @Id Long id, @NonNull @NotBlank String nro_cuenta);
}